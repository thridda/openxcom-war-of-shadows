# Summary #
------
This is a complete ruleset rewrite. The intent is to provide a sequal between UFO defense and TFTD with elements from the more recent games mixed in and some other random indeas. Sort of an X-com 1.5
In general the idea is to force players into a new tactical approach and provide some sensible storyline continuity. Hopefully the late game drop off in difficulty can be bent a bit to keep things interesting. 

## Features ##
------
* 5 new alien races
* 5 new X-Com Craft
* 9 new Craft Weapons
* 8 new UFO's
* 4 new HWP units
* 8 new armor Selections
* 22 new weapons and items
* Rewritten Research Tree
* New storyline elements

## Future Plans ##
------
Once MapScripts and MissionScripts are closer to final for TFTD rules I'd like to make more substatial changes to the maps and missions. 

## Story Background ##
------
After the first alien war the transitionary force on Cydonia took over the operation of the planets elerium mines. After the threat of alien invasion faded from the public's mind, politicians changed the charter of the Xcom task force, converting it to a private enterprise that would colonize mars and mine elerium. The Marsec Corporation was formed, and nearly all of the former Xcom staff went on to work for Marsec, with several former scientists, engineers and soldiers serving as the executive board.

Decades later mars has past peak Elerium production and is now expending more resources to mine a dwindling supply of the precious element.

The high cost of Elerium has led to hoarding by wealthy companies and governments as well as market speculation that has inflated the cost substantially. The cost is so high now that while there are plasma weapons and anti gravity ships in some countries arsenals it's impractical to deploy them for anything more than saber rattling. Firing a single shot from a plasma weapon costs tens of thousands of dollars and flying an anti gravity ship without purpose could quickly drain a nation of substantial treasure. Nations with significant Elerium reserves could engage in actual conflict with those weapons, but doing so could upset the delicate balance of opposing forces. So the world is currently engaged in another cold war. 

Two members of the original Xcom team however did not follow their brothers and sisters into private industry. They still felt that there was a chance that the aliens would return, and worked tirelessly to preserve what they could of the agency, transferring some of the legal infrastructure to a public / private partnership with a tiny caretakers sum to maintain the bare bones infrastructure.

Last week, a family fleeing local warlords was crossing the desert in an attempt to cross the border into another country to seek assylum. When they arrived they told the authorities about seeing a flying ship moving just above the ground between the dunes. It was quickly confirmed that no nation had launched a ship in the region.

Today, Xcom has been reactivated. We know nothing about this new threat, if indeed it is one. It's up to us to get up to speed now. We have some initial commitments for funds from the worlds nations but we should not count on that funding if we are unable to deliver results.

## Economy Overview ##
------
The economy is intended to be resource poor, with high costs for top tier items. Marsec provides many military items that will be familiar to Xcom players, but they are expensive due to elerium and transport costs.