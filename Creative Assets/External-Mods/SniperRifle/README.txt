
-------------------------------------------------------------------------------------------------------------
					Sniper Rifle addon for OpenXcom
-------------------------------------------------------------------------------------------------------------

Description:

 Simple addon to OpenXCom wich includes military sniper rifles for Xcom agents to use, can be
Purchased right from the beginning of the game.

-------------------------------------------------------------------------------------------------------------

to install:

simply extract to your data folder (e.g., X:\OpenXcom\Data)
add the following line to the end of your options.cfg:

  - SniperRifle_u2s
  
 for the ufo 2 sides version
 or optionally for a customized version:
 
 - SniperRifle_custom
 

*note: for now,you need openxcom 0.9 with the latest git build -> http://openxcom.org/index.php/git-builds/ 
       Options.cfg can be found from your documents/OpenXcom folder.

--------------------------------------------------------------------------------------------------------------

Credits:

Sprites: Toshiaki2115 and Warboy1982
Code:  Toshiaki2115 and Warboy1982
Spanish Translations: Nightwolf
