Anthropod Alien - OpenXcom MOD - by robin

This mod adds a new alien race, the "Anthropods".
These aliens are from X-Com Apocalypse, though the Spitter is
redesigned from scratch.

+ Anthropod alien race
+ Spitter(s) terror unit(s) lol

#=================================================================#
INSTALLATION
1.
unzip inside:
OpnXcom\data\

2.
Just activate the mod from the "Options" menu, inside the game.

#=================================================================#
CHANGELOG
1.5
- spitters no more spit in the same manner,
  when retracted they miss autoshot and get decreased accuracy;

1.4
- merged sprites (thanks Falko!); more new sounds!; !!!

1.3
- tweaked missionWeights; tweaked anthropod health and armor;

1.2
- autopsy now should be researchable, extraStrings ok;

1.1
- typos; raceweight tewaked; 

1.0
- I haven't even tested;

#=================================================================#
CREDITS
Dioxine
	The sound of the acid hit.
Falko
	The best tools from the Python Wizard King.