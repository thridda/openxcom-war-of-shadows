
-------------------------------------------------------------------------------------------------------------
					Combat Knife addon for OpenXcom
-------------------------------------------------------------------------------------------------------------

Description:

 Simple addon to OpenXCom which includes a Combat knife, the Daichi Blade.
 can be purchased right from the beginning of the game.

-------------------------------------------------------------------------------------------------------------

to install:

simply extract to your data folder (e.g., X:\OpenXcom\Data)
add the following line to the end of your options.cfg:

  - CombatKnife

*note: for now,you need openxcom 0.9 with the latest git build -> http://openxcom.org/index.php/git-builds/ 
       Options.cfg can be found from your documents/OpenXcom folder.

--------------------------------------------------------------------------------------------------------------

Credits:

Sprites: Chiko and Moriarty
Code:  Warboy1982
Spanish translations: Nightwolf