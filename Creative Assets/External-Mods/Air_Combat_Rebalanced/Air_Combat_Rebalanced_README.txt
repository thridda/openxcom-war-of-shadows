Air Combat Rebalanced
Version 1.0 released 05.07.2014

This mod changes the air combat balance, aiming to make UFOs more challenging, provide incentive to use different weapons together on one craft, make more craft weapons useful and so on.

Changelog:

* Version 1.0 released 05.07.2014
Initial release of the mod


Installation instructions:

As usual, extract the contents of the archive to your "data" folder, merging folders if your OS asks for it. Then enable the mod from the ingame mod menu.

It is recommended to use these advanced options and base mod settings:
Force Craft Launch : YES
Custom Initial Base: YES
UFOExtender Starting Avalanches: NO

If you have the "Raven Interceptor" mod already installed, you are advised to disable it (there should be no compatibility issues per say, but you would encounter duplicates of the Raven itself and other nonsense). You should of course first sell off your Raven interceptors or cancel the relevant research project if you already have a game in progress, in order not to lose scientists, craft weapons and money.


Mod Description:

We all love X-Com for the immensely deep, diverse and challenging tactical combat. However, the air combat in the game is unfortunatlely lacking. This mod aims to improve that.


What is wrong with Air Combat in Vanilla X-Com?

Interceptor equipped with two Avalanche Launchers outranged every UFO in the game except the Battleship (which cannot be outranged by any other weapon), therefore you just used 1 to 3 Interceptors to take down any craft (except the Battleship) with no damage sustained (only Supply Ship required 3, and only Terror Ship required 2). This meant that right from the beginning, you can take down any UFO that matters except the very best with your starting compliment of two Interceptors! Not so tough, are they, those alien overlords with superior extraterrestrial technology?

Two other starting craft weapons, the Craft Canon and the Stingray Launcher, serve no purpose whatsoever (as does the Laser Cannon). Plasma Beam will actually just reduce the amount of Interceptors requried to 1 in all abovementioned cases, and remove the need to reorder ammunition. It has no significant effect on the Air Combat balance (you were already overpowered, now you just don't have to micromanage your supplies anymore). Fusion Ball Launcher, the remaining craft weapon, is also inferior, because after you incorporate the aim bonuses of firing at a Battleship, and the time it takes for the ball to arrive to the target, turns out it actually deals less damage per second than a Plasma Beam! Not to mention limted and expensive ammunition...


How does this mod change things around?

By significantly reducing initial Interceptor HP and starting craft weapons damage output, you are challenged in the air even by Scouts (it would likely require a coordinated effort of both of your Interceptors to take one down).

Two additional "tiers" of air combat are introduced - after researching Alien Navigation (at a reduced required time, so you can do it with small amount of scientists) you will be able to improve your starting weapons' aim, and after researching Alien Alloys, you will be able to research an improved Interceptor with more HP and better speed (it will still have a rent, though).

Avalanche missiles were redesigned: they are no longer viable against smaller targets (because they would just obliterate them - nuclear warheads y'know), and they cost much more per warhead. Additionally, you need at least three interceptors to have a reliable chance to shoot down a Terror Ship. They still outrange everything but Battleships, but now you would probably use them only as a weapon of last resort, when you absolutely have to prevent a terror mission.

Researchable craft weapons have been fine-tuned, so that each serves a purpose:
- Laser Cannon is now a viable craft weapon that sports more comfortable (but still limited) range, rivals Plasma Beam in damage dealt per second, and has enough ammo to take down any UFO.
- Plasma Beam no longer has a chance to destroy scouts, but has limited ammo (still free to recharge, but you might want to bring a backup weapon with you in the second slot).
- Fusion Ball Launcher now outranges even Battleships (but costs elerium to use, of course).

Elerium-fueled craft have seen some changes:
- Firestorm has got a small boost of speed, and a twofold increase in Elerium efficiency compared to an Avenger (after counting in speed differences), making it more viable to replace your Interceptors with them, and making them more resource-efficient at this duty than Avengers.
- Lightning utilises experimental elerium reactor technology, making it require no elerium to refuel (but some elerium to manufacture), this should make it a viable replacement for Skyranger, maybe now it will be of use at least to some commanders.
- Alloy, Navigation and Power Source requirements for all alien technology-based craft have been increased (I mean, they were basically irrelevant, they had to go up to have at least some meaning)

Overall, you will be more tempted to combine different weapons on one craft (layouts like Cannon+Stingray or Laser+Plasma will now make sense), you will spend more time taking fire from UFOs rather than picking them from a safe distance, you will feel more challenged by the alien UFOs and you will have more tools at your disposal to deal with them. Long-range weapons have limited ammo, while short-range weapons can take down any foe (and you will usually have enough HP to afford taking fire). Rearming situational weapons (Avalanche, FBLs) takes 1 hour for whole clip, while rearming beam weapons takes more time than before to make rearm time meaningful.

Finally, no UFO statistics were altered by this mod. You can still expect them to have vanilla statistics you're used to (and refer to popular websites like ufopaedia.org). This is because you do not have a reliable way of acquiring this information ingame (as you can only randomly get it when interrogating Navigators). Changes made by this mod only affect X-Com's craft and weapons, and will be visible in the UFOpaedia or otherwise obvious from looking at ingame screens.


Suggested advanced options and base mods:

Force Craft Launch : YES
Custom Initial Base: YES
UFOExtender Starting Avalanches: NO


Detailed list of changes:

- Interceptor
Was: 100 HP, $600'000 Cost/Rent, 96h Delivery
Now: 40 HP, $400'000 Cost/Rent, 48h Delivery
Scouts are now challenging early game, while reduced cost and delivery time compensates for increased chance to lose the craft early in the game. Reduced rent also makes it viable to keep employing Interceptors to engage smaller craft or serve as long range support with FBLs or Plasma Beams, even after you research Ravens.

- Raven (Improved Interceptor)
New: 220 HP, 3300 Speed, $600'000 Rent, $650'000/40A/30 space/1200h to manufacture
Available after Alien Alloys, has reasonable speed improvement for better response times to alien threat and improved HP to survive engagements with larger UFOs. Costs rent equal to vanilla Interceptors and reasonable manufacturing time even with one workshop.

- Firestorm
Was: 4200 Speed, 20 Fuel, 5 Fuel per 1 Elerium
Now: 4800 Speed, 36 Fuel, 12 Fuel per 1 Elerium
Firestorms served no purpose whatsoever, as the Avengers were faster, more durable and more Elerium-efficient. With the introduced changes, Firestorm should be of more use now.

- Lightning
Was: Uses Elerium as fuel (having worst efficiency amongst all Elerium-fueled craft)
Now: Does not require fuel (still needs "refueling" time though, but much less than terrestrial craft), 2x Repair rate
There is no simple way to make this abomination actually useful - if you're already paying Elerium for your dropship, you'd rather pick an Avenger, and otherwise, the Skyranger beats it in terms of capacity. Requiring neither Elerium nor rent will probably make it useful as a secondary troop transport, for those commanders who prefer to use more than one tactical team. Also, twice the repair rate compensates for the fact that it can only use one weapon.

- Stingray
Was: 70 Dmg, 70% Accuracy, 30 Range, 6 Ammo, 32/24/16s Reload
Now: 40 Dmg, 50(20)% Accuracy, 25 Range, 6 Ammo, 32/24/16s Reload
Alternative to Cannon versus Scouts. See for yourself which you prefer, or maybe even try to equip them both on one craft.
Note: All starting weapons begin with a reduced accuracy (values in brackets), researching UFO Navigation opens up a topic to improve that.
Note: Actual chance to hit a UFO varies based on it's size - weapon's listed Accuracy value is increased by 25% against Large or 100% against Very Large, and decreased by 12,5% against Small or 20% against Very Small.

- Avalanche
Was: 100 Dmg, 80% Accuracy, 60 Range, 3 Ammo, 48/36/24s Reload
Now: 280 Dmg, 40(20)% Accuracy, 45 Range, 2 Ammo, 48/36/24s Reload
A huge nuclear warhead, it has a lower chance to hit but immense damage potential. Using it versus Scouts would be unwise, as you would lose out on Ground Recovery missions. Coupled with the increased missile cost, it should now be a weapon of last resort against larger UFOs thay you have to stop no matter the cost. Both missiles are armed in 1 hour, making it feasible to keep some at every base to rearm your craft in case a larger UFO appears.

- Craft Cannon
Was: 10 Dmg, 25% Accuracy, 10 Range, 200 Ammo, 2s Reload
Now: 10 Dmg, 35(15)% Accuracy, 9 Range, 100 Ammo, 2s Reload
Since you should expect to be in range of enemy craft more often, the pitiful range of the cannon is no longer as limiting factor. It also deals damage faster than Stingray and carries more damage potential (Damage x Accuracy x Ammo) overall, so maybe you should give it a try.
Note: Ammunition for Cannon had its delivery time fixed (now takes 48 hours like other craft ammo)

- Laser Cannon
Was: 70 Dmg, 35% Accuracy, 21 Range, 99 Ammo, 12s Reload
Now: 80 Dmg, 80% Accuracy, 35 Range, 40 Ammo, 10s Reload
The notorious manufacturing topic will now also hopefully see some action. Improved range reduces the delay before it can actually begin firing on bigger UFOs, while improved damage and hit chance brings it up to par with the Plasma Beam in terms of damage per second. Since Plasma Beam has a limited ammo supply now, it might be a good idea to equip at least one Laser Cannon on your craft.

- Plasma Beam
Was: 140 Dmg, 50% Accuracy, 52 Range, 100 Ammo, 12s Reload
Now: 100 Dmg, 50% Accuracy, 55 Range, 10 Ammo, 12s Reload
Reduced damage can be considered a boon, as it will no longer destroy Scouts. Even with limited ammunition, one beam is still enough to crash or severely cripple a Terror Ship. However, you can't just go twin Plasma Beams on every craft, unless you plan on never engaging solo vs Supply Ships and Battleships.

- Fusion Ball Launcher
Was: 230 Dmg, 100% Accuracy, 65 Range, 2 Ammo, 32/24/16s Reload
Now: 300 Dmg, 200% Accuracy, 69 Range, 2 Ammo, 48/36/24s Reload
There was no reason to employ this weapon before. It is now The King of air-to-air weapons, allowing you to kill even Battleships without any damage sustained... but it will probably take you about 48 Elerium to take down a ship you'll net about 50 Elerium from, so you may still want to use weaker weapons. As with Avalanche, it takes 1 hour to rearm fully.

- Research
UFO Navigation: 450 -> 250 hours (to make getting improved accuracy easier)
UFO Power Source: 450 -> 650 hours (to compensate for the UFO Navigation change)
Improved Craft Accuracy: 200 hours, after UFO Navigation, allows you to improve your starting craft weapons' accuracy
Improved Craft Armor: 200 hours, after Alien Alloys, opens up a research topic for the new Raven interceptor.
New Improved Interceptor: 200 hours, after Improved Craft Armor, allows manufacture of the Raven interceptor.


Credits:

- "Raven Interceptor" mod by MickTheMage (http://www.openxcom.com/mod/raven-interceptor)
With the author's permission, the "Raven Interceptor" mod was incorporated into this mod. If you are using the "Raven Interceptor" mod, your would probably not experience any problems or crashes, but you should really disable it, or you will encounter duplicates. Raven's images have also been slightly edited in geoscape and base screens (you can copy over images from the original mod if you like them more).


Feedback:

Your comments, opinions and suggestions are welcome, please post them here (or at the forums).