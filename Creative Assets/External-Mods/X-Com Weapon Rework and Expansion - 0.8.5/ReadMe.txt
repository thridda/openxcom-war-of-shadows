X-Com Weapon Rework and Expansion - Version 0.8

At the moment, aliens will only get the new weapon verities if item levels, alien deployment and all alien weapon modules are enabled.

ChangeLog:

0.8.4:
- Graphical fixes and modularization
- Shotgun added balence pending

0.8.3:
- crash by string fixed

0.8.2:
- directery errors fixed

0.8.1:
- Bug Fixes

0.8:
- All weapons implimented in rudimentry forms
- Item levels and deployment

0.1:
- Visuals only