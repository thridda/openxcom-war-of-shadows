THE FINAL MOD PACK 0.9.7.1

by Open X-Com community
Collected by: Human Ktulu and Solarius Scorch

WARNING: Requires nightly build version openxcom_git_master_2014_09_14_1515 or later! Get it from http://openxcom.org/git-builds/.

WHAT DOES THIS MOD DO?
It adds lots of new content: alien races, missions, weapons, maps, you name it. And everything was made by our community members.

WHY WAS IT MADE?
This megamod has two goals in mind:
1) To include as much extra content made by the community as possible without going overboard, as well as ensure that this content is balanced.
2) To make the game take longer, so that various tiers of equipment and aliens lasted long enough to appreciate them.

HOW DO I INSTALL THIS MOD?
- Open the "OpenXcom\data" folder,
- Copy the "Rulesets", "Resources", "MAPS", "ROUTES" and "TERRAIN" folders to the "OpenXcom\data" folder, merging with existing folders with the same name (some files will be overwritten, so you may want to back them up),
- Enable the mod from the Game Options menu.

WHAT OPENXCOM OPTIONS SHOULD I USE?
- Make sure you use the "item is destroyed on research" option, or else you will be able to research the Alien Data Slate over and over, which would be a cheat.
- Please note that all weapons were balanced for "UFO Extender Accuracy" option turned on, since it makes combat more interesting and varied. You don't have to do it, but it's recommended.

HOW DO I PROVIDE FEEDBACK?
Please post any feedback - bug reports, tanks, criticisms, requests for enhancements and the like - on the following thread:
http://openxcom.org/forum/index.php?topic=2027.0
Or simply in the OXC Mods comment section.

SO WHAT IS INSIDE, EXACTLY?
This can be split into two categories: content that was added from other mods (sometimes slightly tweaked) and the framework that makes it viable. For the former, check the list of included mods below; for the latter, it mostly concerns changes to tech tree.

IS THIS MOD FOR EVERYONE?
No mod is for everyone, because some compromises always have to be made. For example, we do not include content that would invalidate any vanilla content, because we don't want to take away what people already know - only add new stuff. Therefore you may find that some of our decisions were not what you expected. Still, this is a monumental work adding and balancing everything, which you don't have to do yourself, so it's probably worth it!

IS THIS MOD COMPLETE?
No it's not, since we're planning to continuously add more content, fix and bugs and change what's not popular. Also, since this is still a work in progress, there will be some glitches, but probably nothing that could break your game.

IS THIS MOD COMPATIBLE WITH OTHER MODS?
Generally, yes. However, since this is so complex, there is a significant risk that it will conflict with something else. It certainly works with all rulesets released with the game, like the XComUtil elements.
Before applying the mod, make sure it's not already included in the package! If it is, expect a conflict of some sort.

ANY TIPS ON HOW TO PLAY?
The only major difference between playing vanilla game and the Final Mod Pack is access to new research. The FMP tech tree relies rather heavily on live alien interrogation, so capturing aliens - especially higher ranks - is essential. Do not expect to get even to lasers without some investment in live alien capture equipment!
Also, build radars to cover all continents ASAP. The Men in Black bases are particularly nasty, since they may operate for months before discovering them.
Finally, prepare for a longer war than usual. This can't be finished in three months of game time, and I doubt you'll manage within a year.

ANY FUTURE PLANS?
Yeah, a better readme. :P Certainly more language versions, if they come to us. And of course we will continue with upgrading and expanding the mod, since our community is tremendously talented!

CHANGELOG:
0.9.7.1: New Landmine graphics (by yrizoud). Fixed some units' height to make them not crash the newest nightly. Fixed Alien Drone hit animation.
0.9.7: Some more UFO maps added from grzegorzj's Yet More UFOs! collection. Some ruleset cleanup. Laser Cyberdiscs are now orange, with orange weapon sprite. Added Medikit hand sprite by Ivan Godovich. Added some urban map blocks I made in spare time. Using MiB armours now requires MiB Activity researched. Fixed erroneus starting ammo. Dogs have Bravery 10 now and are more expensive.
0.9.6.1: Minor text fixes. Cerebreal Larva crash fix.
0.9.6: Added a late variant of the MiB (credits to Falko - Sectopod, Shadics - Power Suit and XOps - Stormtrooper Armour). Fixed prerequisites for Auto-Cannon and Heavy Cannon alloy clips. Armour translations stuff fix. Enhanced sprites for the Jump Armour (by Xeno Viper). Fixed a broken string for the AA Pistol Clip. TU cost for using knives is flat rate now. Laser weapons and the Fusion Torch are easier to develop. Rebalanced alien armours a little. Introduced Elerium extraction from alien clips and explosives (based on Falko's mod). Minor tweakings of alien races composition. Added Alien Raiders as a (unlikely) race for some standard missions. Fixed missing Assault Rifle description. Fixed Hovertank/Launcher Ufopedia page. Blaster Bomb costs 5 Elerium now, Fusion Ball costs 8 Elerium. Power armours and most tanks are now somewhat weaker to lasers.
0.9.5: Introduced various colours of Waspites. Fixed some bugs regarding new armours. Renamed Alloy Skyranger to Skymarshall. Gauss tank shells are cheaper. Added Assault Rifle (sprites by XOps, slightly altered) which uses Rifle clips. Modified Stormtrooper Armour - new helmet and is now black. Fixed Alloy Clip for the Rifle, as it had 30 bullets instead of 20 (why nobody has noticed this?). Updated Hobbes' Mission/Terrain Pack mod. Fixed the Muton Guard crash.
0.9.4: Finally coded the Alien Electronics component, necessary for building Sectopods and Stormtrooper Armours (and possibly more in the future) and only available by disassembling alien robots. Made Auto Cannon, Heavy Cannon and Proximity Mine researchable (but cheap). Added Tactical Sniper Rifle (sprite by Dioxine). Added Landmine (available to buy). Added new Stormtrooper Armour (sprites by XOps). Added Multi-Launcher (sprite provided by RSS Wizard). Added Advanced Rocket launcher (sprite ripped from Shadow Warrior, converted by Dioxine) that fires Large Rocket, Elerium Rocket (ripped from Xenonauts, converted by Dioxine) and Mind Guided Rocket (by Clownagent, formerly launched from Rocket Launcher). Added special sprites for Sectoid Commander (made by XOps), Muton Engineer (made by Dioxine) and Floater Commander. Added new Muton ranks: Praetorian and Guard (sprites by XOps). Added a machete for hybrids and other rabble (courtesy of Niculinux). Salamandron can bite now (beta feature, we'll see if it works properly). Shorter arrival time for craft cannon rounds. Alloy Sword takes more storage space. Elerium Mace a bit more accurate, but requires Alien Subjugation. Removed two faulty UFO maps. Tank/Laser Cannon is a separate (cheap) research now, because of new coding possibilities with the latest nightly that will soon be used. Updated Hobbes' Mission/Terrain Pack mod. Fixed a bug with drone smoke launcher. Fixed new UFO maps by Grzegorzj. More English fixes by Jayden.
0.9.3: Changed Scatter Laser's sprites to one drawn by Rockfish, because the former will be reused as something else. Some armour stats juggling. Corpse weights adjusted. Added Alloy Vest. Added Alloy Knife. Better image for the MiB Heavy Laser (by Dioxine). Made image for the Stormlance. Toxigun Flask can now be researched without the Alien Gardens tech (but not manufactured). Updated Dioxine's Improved Living Quarters mod to the latest version. Many English fixes, by Jayden.
0.9.2: Nerfed down the Absurdly Sharp Alloy Sword. Added the Laser Shotgun, by popular demand (sprite by Warboy, I think).
0.9.1: Hybrid research crash fix. Ironfist map fix. Hovertanks ufopaedia entries fixed. Added Improved Living Quarters mod by Dioxine and changed starting base layout. Added Alloy Sword by NeoWorm.
0.9: Rebalanced shotguns to make them more profiled. Fixed Stormlance missiles, so that they're no longer buyable. Fixed hybrids, who used to be heavily armoured for no reason. Updated Hobbes' Mission/Terrain Pack mod. Added Tactical Lightning mod by x60mmx. Added Ironfist Dropship by Solarius Scorch. Better Magnum sound.
0.8: Added Armoured Vest by Robin. Added Hybrid Race and Elerium Missile by Solarius Scorch. Fixed case-sensitiveness and some other minor bugs. Fixed the Scout Drone on Linux and made a bigob for it. Updated Hobbes' Terrain Pack to include a bunch of new terrains. Lasers require Elerium now to produce. Some cleanup of maps and listOrders. Tougher aliens appear later.
0.7: Added First Aid Kit and both X-Com Cyberdiscs Ufopaedia texts. Added Arthropod race by Robin. Added a missing Abductor file (OX_UFO4F.MAP). Fixed drone smoke pellets. Updated Hobbes' Terrain Pack to include the new Railyard terrain.
0.6.1: Added Elerium Bomb mod by Fatrat. Adjusted list orders. Tank/Rocket Launcher has Auto Fire option (2 rockets). Tank/Cannon has explosive rounds. Fixed some vanilla missions possibly not appearing. Hobbes' Terrain Pack updated to new version. Reworked flying armours for better logic. Added a Laser Sniper Rifle.
0.6: Many pictures added to the new alien research items, mostly by our very talented Vulgar Monkey. Gazer mod update. Fixed MiB tank.
0.5.6b: Minor fixes.
0.5.6: Men in Black updated. Labship and Sentry Ship maps fixed. Some work done on the laser tech tree.
0.5.5b: Lol, forgot the floorobs for the new lasers. Fixed now.
0.5.5: Added Expanded Ubase mod. Added Hazmat Armour mod. Added X-Com Cyberdisc (green sprite provided by Civilian). Changed Men in Black lasers to clip versions. Moved sprites to sprite sheets in order to save disk space and make loading faster.
0.5.4b: Oops, errors in the tech tree. Download this to be able to research important stuff.
0.5.4: Made personal laser/gauss weapons a bit easier to get. Adding size to X-Com corpses. Incendiary Grenades have larger blast radius. Fixed a bug with the Gauss Cannon Ufopaedia entry. Upgraded from newest versions of Hobbes' Terrain Pack and Solarius Scorch's Alien Armoury Expanded mods.
0.5.3: Actually adding the slate to the aliens' inventories lol, as well as to alien computers in bases (yay, new options), listOrder fix.
0.5.2: Introducing Alien Data Slates, maps update by Hobbes.
0.5.1: Bringing the mod to the latest nightly build (openxcom_git_master_2014_07_03_0820) - related to the change of original tech tree, Medipack crash fix, Scout Drone now shoots in arcs.
0.5: First release of the megamod.

DISCLAIMER:
All content belong to their respective creators, as indicated in the list of included mods. This is merely a compilation, binding this content together and balancing its stats to form a smooth gaming experience.
I did what I could to credit everyone properly, but mistakes happen. If your content is here and you are not credited, please contact me and I'll fix it really fast!

SPECIAL THANKS:
- Warboy and SupSuper, for bringing the OpenXCom project to life and suffering the modders' whining.
- Falko, for his invaluable ruleset and sprite tools.
- All the talented translators, including but not limited to: Aldorn, Bones, LouisdeFunes, MickTheMage, Nightwolf, Phobos2077 and Rewkha.

Let us know what you think - we'll work on further improvements, after all this is the first release. But first and foremost, have fun playing this!
	- Solarius Scorch


---=== LIST OF INCLUDED MODS ===---

* Combat Knife, by Warboy1982
http://www.openxcom.com/mod/combat-knife
Adds a combat knife.

* Dart Rifle, by Solarius Scorch
http://www.openxcom.com/mod/dart-rifle
Adds a ranged stun weapon.

* Elerium Mace, by Tyran_Nick
http://www.openxcom.com/mod/elerium-mace
Adds a melee stun weapon.

* Flamethrower, by Dioxine
From the Piratez mod: http://www.openxcom.com/mod/piratez
Adds a flamethrower.

* Fusion Torch, by Ran
http://www.openxcom.com/mod/fusion-torch
Adds an advanced UFO cutter.

* Gauss weapons (and a new craft cannon), by Solarius Scorch (GFX by GrandSirThebus)
http://www.openxcom.com/mod/gauss-mod
Adds gauss weapons for soldiers, tanks and craft.

* Grenade Launcher, by Warboy1982
http://www.openxcom.com/mod/grenade-launcher
Adds a grenade launcher with several ammo types.

* HMG, by Dioxine
http://openxcom.org/forum/index.php?topic=1922.msg18049#msg18049
Adds a heavy machine gun with high power, good accuracy and poor mobility.

* Incendiary Grenade, by Simon-v
http://www.openxcom.com/mod/custom-grenades
Adds incendiary grenades.

* LMG, by Ryskeliini
http://www.openxcom.com/mod/ryskeliinis-guns-n-gadgets-1st-pack
Adds a light machine gun - stronger but not as precise as the rifle.

* Magnum, by Ryskeliini
http://www.openxcom.com/mod/ryskeliinis-guns-n-gadgets-1st-pack
Adds a big pistol.

* Mind Missile, by Clownagent
http://www.openxcom.com/mod/mind-missile
Adds a new rocket for the launcher that uses blaster launcher mechanics.

* Minigun, by Solarius Scorch
http://www.openxcom.com/mod/minigun
Adds 3 minigun weapons and a new tank.

* Shotgun, by Warboy1982 and Dioxine (a patchwork of their mods)
(no link available)
Adds a shotgun.

* SMG, by Ryskeliini
http://www.openxcom.com/mod/ryskeliinis-guns-n-gadgets-1st-pack
Adds a sub-machine gun. Brrrrrt!

* Sniper Rifle, by toshiaki2115
http://www.openxcom.com/mod/sniper-rifle
Adds a precision rifle.

* Taser, by Ryskeliini
http://www.openxcom.com/mod/ryskeliinis-guns-n-gadgets-1st-pack
Adds a stun gun.

* Alloy ammo, by Moriarty and Solarius Scorch
(no link available)
Adds alien alloy ammunition for all firearms (and a new craft cannon).

* HWP.rul, by Harry
http://www.openxcom.com/mod/sectopod-hwp
Adds Sectopod chassis to be used by X-Com.

* Scoutdrone.rul, by Arpia
http://www.openxcom.com/mod/scout-drone
Adds a small scouting vehicle.

* First Aid Kit, by Yrizoud
http://openxcom.org/forum/index.php?topic=1782.0
Adds a big, heavy medikit (the vanilla medikit must be researched).

* Plasma Beam Mod, by Solarius Scorch
http://www.openxcom.com/mod/plasma-beam-mod
Modifies Plasma Beam craft weapon so that it uses Elerium-115 as ammunition: each shot costs one Elerium. "Ammo" capacity was also decreased from 100 to 50 shots.

* Alien Melee Stat, by Solarius Scorch
http://www.openxcom.com/mod/alien-melee-stat
Changes Melee stats of aliens to more reasonable values.

* Alien Armoury Expanded, by Solarius Scorch
http://www.openxcom.com/mod/alien-armoury-expanded
Adds new weapons and UFOs, as well as makes the aliens retain their smaller weapons throughout the game.

* Alien Inventory Mod, by Cyrix
http://www.openxcom.com/mod/alien-inventory-mod
Adds inventory pictures for mind-controlled aliens.

* Laser Sectopod, by Louis de Funes
http://openxcom.org/forum/index.php?topic=1958
Changes Sectopod weapon description to laser, according to the lore.

* Muton Commander, by Phaedris
http://www.openxcom.com/mod/muton-commander
Adds Muton Commanders and Muton Leaders (as in the game intro).

* Recycled Alien Collection, by Solarius Scorch and Civilian
http://www.openxcom.com/mod/recycled-alien-collection
Adds new aliens, using recoloured sprites from X-Com: Terror from the Deep.

* Terrain Pack, by Hobbes
http://www.openxcom.com/mod/terrain-missions-pack
Adds new terrains to fight on, as well as new alien missions.

* Stun Grenade, by Murmur
http://www.openxcom.com/mod/stun-grenade
Allows converting stun bombs to stun grenades.

* Men in Black, by Robin
http://openxcom.org/forum/index.php?topic=2180.0
Adds a human faction to fight with.

* Gazer Alien, by Robin
http://openxcom.org/forum/index.php?topic=2180.0
Adds a new alien race.

* Cover Alien, by Robin
http://openxcom.org/forum/index.php?topic=2180.0
Adds a new alien race, based on the game's cover art.

* Anthropod, by Robin
http://openxcom.org/forum/index.php?topic=2180.0
Adds a new alien race, inspired by the X-Com: Apocalypse.

* Alloy Skyranger Mod, by Civilian
http://openxcom.org/forum/index.php?topic=2048.0
New Skyranger map (+2 capacity).

* AlloyCrafts, by The Old One
(no link available)
New variants of Interceptor and Skyranger.

* Raven, by MickTheMage
http://www.openxcom.com/mod/raven-interceptor
Adds a new fighter plane.

* Sentinel, by Warboy1982
http://www.openxcom.com/mod/sentinel
Adds a new fighter plane.

* Thunder mod created by Tyran_Nick
http://www.openxcom.com/mod/thunderstorm-interceptor
Adds a new fighter plane.

* Firestorm Restyle, by Shadow
http://www.openxcom.com/mod/firestorm-restyle
Effectively, adds a new fighter plane.

* Alien Base Cave, by Civilian
http://openxcom.org/forum/index.php?topic=2091.msg20402#msg20402
Adds a new type of alien base.

* Alien Base Addon Terrain, by Civilian
http://openxcom.org/forum/index.php?topic=2082.0
Adds new mapblocks to the alien base.

* Farm Addon Terrain, by Civilian
http://openxcom.org/forum/index.php?topic=2082.0
New barns and stuff.

* Reproduction, by Warboy1982
http://www.openxcom.com/mod/alien-reproduction
Allows access to the alien reproduction research topic.

* Weapon Sound Variety, by Tarvis
http://openxcom.org/forum/index.php?topic=2045
Adds more varied plasma sounds.

* Expanded Terror, by Luke83
http://www.openxcom.com/mod/lukes-expanded-terror-mission
Adds new city mapblocks.

* Luke's Extra UFOs, by Luke83 and TurkishSwede
http://www.openxcom.com/mod/lukes-extra-ufos
Adds new UFO maps.

* MediGas, by Roxis231
http://openxcom.org/forum/index.php?topic=2446
Adds grenades to knock out aliens.

* Grav Armors, by Clownagent, Moriarty and Solarius Scorch
http://openxcom.org/forum/index.php?topic=2465
Adds new flying armours, as well as mechanics for repairing damaged armours.

* Tank.rul, by Arpia
(no link available)
Adds more tank types.

* Firestorm Gfx, by Tyran_Nick
http://openxcom.org/forum/index.php?topic=1435.0
Changes the Firestorm picture.

* Hazmat Armour, by Solarius Scorch
http://www.openxcom.com/mod/hazmat-armour
Adds a fireproof armour.

* Expanded U_Base, by Luke83
http://www.openxcom.com/mod/expanded-ubase
Adds new alien base blocks.

* Elerium Bomb, by Fatrat
http://openxcom.org/forum/index.php?topic=2561.0
Adds new ammo type for the Small Launcher.

* Armoured Vest, by Robin (reskinned after Ryskeliini)
http://www.openxcom.com/mod/armored-vest
Adds a buyable starting armour.

* Ironfist Dropship, by Solarius Scorch
http://www.openxcom.com/mod/ironfist-dropship
Adds a new troop transporter.

* Tactical Lightning, by x60mmx
http://openxcom.org/forum/index.php?topic=2634.0
Makes the Lightning a bit more useful.

* Improved Living Quarters, by Dioxine
http://openxcom.org/forum/index.php?topic=2806.0
Adds bathrooms to Living Quarters and a new, large type of Living Quarters.

* Alloy Sword, by NeoWorm
http://openxcom.org/forum/index.php?topic=2819.0
Adds a new melee weapon.

* Break 2 Elerium, by Falko
http://openxcom.org/forum/index.php?topic=2055.0
Enables disassembling alien clips and explosives for Elerium.

* MediKit HandOb, by Ivan Godovich
http://www.openxcom.com/mod/medikit-handob
Adds a hand object for the Medikit.

And various graphic assets from various talented people, most notably:
* Vulgar Monkey
* Dioxine
* Civilian
* Rockfish
* Ryskeliini
* XOps