
-------------------------------------------------------------------------------------------------------------
					COLORED MORIARTY ARMORS
-------------------------------------------------------------------------------------------------------------


Description:

 Simple OpenXCom mod which give some colors to the famous Moriarty armors.

-------------------------------------------------------------------------------------------------------------

To install:


- Extract the .zip to desktop. 

- Choose your favorite colors for the Power and Flying armors. 

- Copy the files from the color folders of both armors to "Resources\ColoredMoriartyArmors\images\power" and "Resources\ColoredMoriartyArmors\images\flying". 

- Copy "Resources" folder to OpenXCom data folder (e.g., X:\OpenXcom\Data)

- Copy the desired ruleset file (helm or no-helm version) to OpenXCom Ruleset folder (e.g., X:\OpenXcom\Data\Ruleset)




--------------------------------------------------------------------------------------------------------------


Credits:


Base Armor: Moriarty.
