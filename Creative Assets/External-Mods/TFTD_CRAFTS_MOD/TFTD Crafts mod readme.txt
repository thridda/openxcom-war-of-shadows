X-Com TFTD Crafts remake\reskin mod

Changelog:
-Slightly increased size of all crafts
-increased ramp width by one square for all crafts (for rapid deployment)
-added new interior for all crafts
-slightly modified exterior for Triton (engine nozzles) and Hammerhead (two front searchlights-8 squares range)

Compatibility:
Mod compatible with any version of the game and with any mod which dont change files:
SCANG.DAT
HAMMER.MAP
LEVIATH.MAP
TRITON.MAP
HAMMER.MCD
HAMMER.PCK
HAMMER.TAB
LEVIATH.MCD
LEVIATH.PCK
LEVIATH.TAB
TRITON.MCD
TRITON.PCK
TRITON.TAB

Installation:
Extract "GEODATA", "MAPS", "TERRAIN" folders from archive to your root game directory.

--------------------------
Thanks to Koralt and Ben Ratzlaff for tools