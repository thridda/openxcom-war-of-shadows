Elerium Mace addon for OpenXcom

Description:

This addon includes the Elerium Mace melee weapon.
This can be researched once Elerium-115 has been investigated.

Installation:
Extract to your data folder (.\OpenXcom\Data)

and add the following line to your config.cfg

- EMace

Changelog:
- Updated string localisation

Credits:
tyran_nick