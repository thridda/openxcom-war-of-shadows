Readme

Xeno Operations v0.8 Beta
By XOps

What This Is
Xeno Operations is a large expansion mod for OpenXcom. Basically it is a bunch of stuff I wanted to see in the game. To be perfectly honest, most of this is stuff other mods have already added. There isn't much original content here save for some new alien gear and a couple of odd items here and there. A lot of graphics have been tweaked though. I have included a spoiler list of everything added in all rulesets at the end of this readme. Those who wish to be surprised should avoid that section.

How to Install
Copy all files to the Open XCom data directory. Overwrite the folders, but don't worry. This mod shouldn't replace any original files. Be sure to use the latest nightly.

Understanding the Rulesets
I have kept the rules as modular as I can so players can pick and choose which parts they want.
XenoOpsCrafts: Adds new crafts and craft weapons as well as producible goods to sell.
XenoOpsAlienArsenal: Adds a bunch of alien weapons, and gear to the game. Warning: Makes the game more difficult.
XenoOpsAliens: Adds a couple of new aliens to the game as well as some new takes on old foes. Warning: Makes the game more difficult.
XenoOpsArmors: Adds a bunch of armor to the game from starting kevlar all the way to stat boosting power armor. It only removes Personnel Armor which it replaces with the slightly better Alloy Armor.
XenoOpsWeapons: Adds a lot of weapons to the game as well as an extra tier of weapons before lasers. Warning: Makes the game more difficult as it takes longer to get to lasers.
XenoOpsFacilities: Adds new facilities and maps for those facilities.
XenoOpsCydoniaChallenge: Modifies the maps for the final mission making them both larger and with more aliens. Use if you want the final mission to be a real challenge.

The Lite Ruleset
XenoOperationsLite: This includes almost everything from the from the above versions, but keeps the pacing closer to the original game. There is no plasma conversion system or ballistic tech tree (the enhanced ballistic weapons are available for construction at the start of the game). However this version does not attempt to balance old game issues like psionics or the blaster launcher. Use this if you want to play with the new toys, but don't want the tedium of the conversation system or the long wait of larger tech trees.

Coming Eventually...
XenoOperations: The main mod. This ruleset combines all of the others and greatly changes the game. It turns the game into a marathon version with a much longer tech tree and attempts to fix balance issues with the original game. This will be the full intended experience.
XenoOperationsHopelessWar: This is a challenge mod. It completely changes the game with the aim of making the game more difficult while also adding a bunch of stuff. This will be extremely difficult and will be intended for die hard veterans who complain about superhuman still being too easy. There will be a lot of new toys, but at the cost of much better old ones. Expect a long and slow tech tree, but don't expect the Aliens to wait for you to catch up.

Recommended Settings
Ranged based accuracy. The mod does take this into account and this option greatly diversifies the weapons.
TFTD Manufacturing Rules. Almost required since this mod has you making a lot of ammo and other items.
Psi-amp requires line of sight if you don't want the game to turn into easy street near the end.
Aliens pick up weapons is unneeded. Attraction is already set for most alien items.
Alien Weapons self destruct makes the game 100 times harder if you use the Alien Arsenal part as every captured alien weapon is broken down for less parts then what is needed to make an equal sized human plasma weapon. Use this setting only if you want a real challenge getting plasma weapons.

Thanks to...
All of the OpenXcom crew for allowing me to play my favorite game again while also giving me the means to tweak it.
Warboy, robin, Tyran_nick, Dioxine, SolariusScorch since I copied/mimicked a lot of their code
Dioxine for letting me steal his expanded Living Quarters and Stores.
Falko for making awesome tools that I used to find a number of errors.
Chiko since I stole parts of his alien plasma weapons.
Anyone else I may have stolen sprites from at one point without realizing it (pretty sure I only stole from Chiko and Dioxine).
Big thank you to pkrcel, Duke_Falcon, cjones, and Arthanor who gave me detailed feedback and ThatDude who found a couple of nasty bugs for me.
Beta Testers and others who gave me feedback: smexyvami,  Mr. Quiet, kkmic,	robin, Recruit69, ThatDude, BlackLibrary, ivandogovich, Dioxine, KingMob4313, the_third_curry, Yankes, Solarius Scorch, Arthanor, Infini, VSx86, XCOMFan419, MFive, cjones, moghopper, SIMON, and Squaddie Perseus.
And
Anyone who gives me constructive feedback, nonconstructive feedback, tries the beta, and/or strokes my ego by telling me my mod is awesome.

Mod Compatibility and Recommendations
Luke's extra UFOs: Works fine.
the Terrain Pack: Works fine though avoid the Mission pack part as the deployments messes with the Alien Aresenal part.
Expanded UBase also works fine however I recommend turning Cydonia Challenge off and then on again to make sure it loads last.
Arthropod, Gazer, and Waspite mods all work fine though the appearance ratios should probably be adjusted and research will not work right.
The Alien Inventory mod works fine, but is unneeded as XenoOpsAliens comes with its own version.
Any mod that adds starting weapons will probably work without problems.

Mods I know will not work
Any megamod such as the Final Mod Pack, Piratez, Xcom Armory Expanded, or Equal Terms
Alien Armory Expanded and Item Levels or any mod that messes with alien deployment gear
The Mission Pack part of the Terrain Pack
The Men in Black Mod because of it's deployments
The Alien Remix Mod because it messes with Sectoid mission ratios. The Sectoid race entry has been changed to Cydonia Landing Guard because the first Cydonia Mission is hardcoded for Sectoids.
Any mod that messes with laser weapons or plasma weapons.
Alternate Weapon Tech since it messes with items that XenoOps has removed from the game.

A few tips
-  The Minigun is very useful for base defense since it is basically an area of effect weapon without an explosion. Park a soldier with a minigun at the end of a choke point and laugh. Otherwise it is very difficult to use and probably the worst weapon to bring to a terror site.
-  Xeno Operations armor is great for rookies. Even though it is very weak, it is a cheap armor that boosts shooting. When the rookie's stats improve, they can graduate to heavier armor.
-  If you use the Alien Arsenal part of the mod, then it is extremely important you don't bunch up. The aliens have a few new area of effect weapons that are very devastating.
-  First Aid Kits are a starting medikit. They are heavy though and are more costly in TUs to use. You should replace these quickly with medikits.
-  Use armor colors for quick identification of soldier roles. For example, dress all of your snipers in blue or put your best troops in black. That way you can know with a quick glance where your soldiers are on the battlefield.
-  Security stations make base defense much easier. In fact I am a bit worried it is a bit of a game breaker in that area.
-  Corridors are awesome for decreasing build times as they are a cheap facility to connect other facilities. Use them to quickly flesh out a base and then take them apart when you don't need them anymore.

Known Bugs
  -Tried to fix Cydonia and Martian Solution techs not showing up after research of the new high ranked aliens. Still not working completely for the Muton Warlord for some reason.
  -Armored Sectoid Commanders and Leaders do not give psilab when researched.
  -Hit animation disabled for antimatter clips since it causes a crash with the latest nightly.
  -Update to the lastest nightly if the game is crashing after hit animations play for melee weapons.
  -Researching plasma conversion says a bunch of research is unlocked when it already was. Don't know how to get around this. It doesn't affect the game in any way currently though.
  -If the Cydonia Challenge does not work right, then reload the the CydoniaChallenge.rul by turning it off and then on again. Sometimes it conflicts with the deployment data for AlienArsenal.rul. In fact you may wish to load CydoniaChallenge last.
  -You can still see and fire through the Skywarden's back door. I don't know why. I will keep messing with the MCD editor to fix this eventually.
  -Enhanced Soldier sprites causes a few issues. I have tried to fix the worst ones.

Warning: ***SPOILERS BELOW***

















Finally a complete spoiler list of changes per ruleset.

XenoOpsCrafts
  -X35 Shield JSF Added as a starting craft.
  -XCF-1 Rook Added. Looks like the original interceptor. Runs on conventional fuels.
  -XCF-2 Guardian Added as a tier two Alloy interceptor. Runs on conventional fuels.
  -XCF-3 Aegis added as a tier three Alloy aircraft.  Requires special fuels, but fuel is non Elerium in nature.
  -Added Cluster Missile craft weapon. A short range rapid fire missile.
  -Added Pilum Missile craft weapon. A long range heavy damage missile.
  -Changed all sprites for all craft weapons in both basescape and geoscape.
  -Added 3 tiers of producible goods used only for sale.
  -Added Skywarden transport.
XenoOpsAlienArsenal
  -Updated all sprites for Alien Weapons. Color changed to match UFOs. Used parts of Chiko's sprites to change them.
  -Added Plasma Assaulter. Basically an Alien SMG.
  -Added Plasma Blaster: Basically an Alien Shotgun.
  -Added Plasma Sniper Rifle.
  -Added Plasma Cannon.
  -Tweaked other plasma weapons.
  -All alien plasma weapons cannot be used by XCom agents in the field even if they have been researched. Xcom must produce their own or covert them from captured Alien weapons.
  -Added human plasma weapons for Pistol, Assaulter, Rifle, Sniper Rifle, Blaster, Heavy Plasma, and Plasma Cannon.
  -Added Overcharged clips for all plasma weapons. Basically does higher damage for less shot capacity.
  -Added Antimatter clips for all plasma weapons. Lower damage but has an area of effect.
  -Changed color of the Alien Grenade. Also gave it a spiffy handob.
  -Added Alien Mine. Basically a more powerful Proximity Grenade. Too bad the aliens are too dumb to use it.
  -Added Alien High Explosive. This stupid thing makes the game much harder. I have had my entire team nearly wiped out by these horrible things. Why did I add this in again?
  -Added Alloy Blade.
  -Added Plasma Sword.
  -Changed color of the Stun Bomb.
  -Changed color of the Small Launcher.
  -Changed Blaster Bomb sprite.
  -Added Elerium Grenade
  -Added Elerium Mine
  -Added Antimatter Bomb
  -Added Human Alloy Blade
  -Added Human Plasma Sword
  -Added Alien Comm. Device. Can be broken down into an Elerium Cell.
  -Added Elerium Lamp. Basically an Alien Electro flare. As in true alien fashion, it is actually better than the human version of course. Showoffs.
  -Added Alien Holographic Projector. Can be broken down into Elerium Cells, and Circuits.
  -Added Alien Injector. No use except as something to sell.
  -Added Alien Small Entertainment. Can be broken down into Elerium Cells, and Circuits.
  -Added Alien Computer. Can be broken down into Elerium Cells, and Circuits.
  -Added DNA Sample. No use except as something to sell.
  -Added Alien Embryo. No use except as something to sell.
  -Added Alien Extractor. No use except as something to sell.
  -Added Organ Sample. No use except as something to sell.
  -Added Elerium Cell component
  -Added Plasma Weapon Component
  -Added Alien Electronic Circuit
  -Added Antimatter Jewel
  -Reworked Conversion System for plasma weapons to make use of the above components as well as allowing some of the alien gear to be taken apart.
  -Added Elite Plasma Rifle. Only used on the final mission by the Aliens. Can be used by humans at the moment.
  -Changed sound for Alien Psi weapon. It is much more creepy now. You're welcome.
  -Gave the Crysalid a nice Melee animation because you know, it wasn't horrifying enough already. Same animation also added to zombie attack.
  -Reapers now have a nice little bite animation for their attack.
  -Changed item levels and added new alien equipment for deployment data for all missions.
  -Different clips for plasma weapons now have different bullet sprites. Also the default is now orange to match with the whole powered by Elerium theme.
XenoOpsAliens
  -Added Overlord Alien Race at long last. This is a 100% original sprite.
  -Added Energy Alien terrorist for the Overlords.
  -Added Muton Commando Race. They are supported by Sectopods because a super tough Muton wasn't nearly bad enough on its own.
  -Added Muton Warlord. Basically a Muton Commander. He deploys in the commander slot of both the Mutons and Muton Commandos. He counts as a commander for research too.
  -Added Armored Sectoids. Supported by Cyberdics and Hybrids. Core stats are similar to Sectoids save for the armor which does boost their stats a bit and is about as tough as alloy armor. I had their armor pretty high, but have lowered down a bit since then.
  -Added Muton Praetorian. These are only on the final Cyndonia Mission. 
  -Added Hybrid Terrorist unit to go along with the Armored Sectoids
  -Updated a few UFOpedia entries with extra artwork: Cydonia or Bust, and the Martian Solution. May fix up others like this in the future.
  -Changed MIXED to Cydonia Elite Guard and changed the race composition.
  -Changed STR_SECTOID to Cyodonia Landing Guard. Replaced old instances of Sectoid with STR_SECTOID_NEW. Had to do this because of hardcoding with the final mission.
XenoOpsArmors
  -Added Kevlar Armor. Pretty crappy, but better than the default coveralls. Plus it looks cooler than coveralls. Comes in six different colors and three different beret styles. Available from the start.
  -Added HEC armor. Basically a hazard suit. Good against acid attacks and smoke damage, but lowers shooting a tiny bit. Only comes in black. Available from the start.
  -Added Xeno Operations Armor. Better than Kevlar and gives a tiny shooting boost. Good for poor shot rookies who you don't want to waste good armor on. Comes in six different colors.
  -Added Alloy Armor. Slightly better than personal to take into account the new nasty alien weapons. Comes in six different colors.
  -Added several colors of personal armor.
  -Added Sky Strike Armor. Not as good as Alloy Armor, but can fly. Cheaper than the Flying Suit as well.
  -Added Nerve Suit. Boosts a bunch of stats. Good for scouts and those with good reactions, but not as strong as Power Armor. Late game. Comes in six different colors.
  -Added Power Armor. Late game. Better than the power suit. Boosts a lot of stats, but hampers Psi Skill while boosting Psi Strength. Very expensive. Must have spent forever getting the helmet look right. Still may need some graphic tweaks though in that area.
XenoOpsWeapons
  -Changed the look of the Pistol as well as the sound. Renamed to service pistol.
  -Added a Machine Pistol
  -Added a Magnum
  -Added a Small Pistol
  -Added an UZI
  -Added a Personal Defense Weapon
  -Added an Assault Rifle
  -Added a Shotgun
  -Added a SMG
  -Added a LMG
  -Added an Anti Material Rifle
  -Added a Sniper Rifle
  -Added a Minigun
  -Added an Incendiary Grenade
  -Added a Combat Knife
  -Added a starting first aid kit
  -Added Brass Knuckles
  -Added a tier 2 Pistol
  -Added a tier 2 Machine Pistol
  -Added a tier 2 Magnum
  -Added a tier 2 Small Pistol
  -Added a tier 2 UZI
  -Added a tier 2 Personal Defense Weapon
  -Added a tier 2 SMG
  -Added a tier 2 Assault Rifle
  -Added a tier 2 Shotgun
  -Added a tier 2 Sniper Rifle
  -Added a tier 2 LMG
  -Added a tier 2 grenade
  -Added alloy ammunition for all tier 2 weapons save for the grenade
  -Added an alloy rocket for the rocket launcher
  -Changed the look of all laser weapons
  -Added battery packs for all laser weapons
  -Added charged cells for all laser weapons
  -Added Elerium batteries for all laser weapons
  -Added Assault Laser. Basically a laser SMG
  -Added Bloom Laser. Basically a laser shot gun
  -Added Laser Sniper Rifle
  -Added a Pulse Laser. Basically a laser LMG
  -Added a Laser Sword
  -Added Hypervelocity Pistol (All hypervelocity weapons are %100 original sprites)
  -Added Hypervelocity Assaulter (Basically an SMG)
  -Added Hypervelocity Rifle
  -Added Hypervelocity Frag Rifle (Basically a shotugn)
  -Added Hypervelocity Sniper Rifle
  -Added Hypervelocity Repeater (Basically an Light Machine Gun)
  -Added Heavy Hypervelocity
  -Added Hypervelocity Cannon
  -Updated bullet sprites.
  -Updated rest of UFOPedia look
  -Updated Cursor Graphics
  -Add melee animation for stun rod
XenoOpsFacilities
  -Added Satellite Uplink Center that allows for global radar coverage. Upgraded to a 2x2 facility.
  -Added Hyperwve Uplink Center that allows for global hyperwave coverage. Upgraded to a 2x2 facility.
  -Changed sprite for Alien Containment
  -Added Large Workshop
  -Added Large Lab
  -Added Large Psi Lab
  -Added Corridor
  -Added Security Checkpoint
XenoOpsHWP
  -Added UAV
  -Added UAV/Rockets
  -Added Tank/Mortar (starting HWP)
  -Added Tank/MRLS
XenoOpsCydoniaChallenge
  -Cydonia maps now a 100x100 for each stage
  -Number of Aliens doubled per stage
  -Good luck

Changelog
V0.90
  -Fixed double personal armor entry
  -Increased Alloy Armor $ Cost
  -Increased Skystrike Armor $ Cost
  -Lowered Elerium Cost for Skystrike Armor to make it more useful
  -Changed rate of fire for Plasma Assaulter. Auto was cheaper than snap shot
  -Fixed plasma sword hand obj to look a little better
  -Lower cost and time to dismantle all plasma weapons and clips
  -Line of sight required built into psi amp in armor ruleset now
  -Fixed Muton Commando Corpse not being researchable
  -Added UAV and UAV/Rockets
  -Added Tank/Mortar
  -Added Tank/MRLS
  -XCF-2 now only has one weapon slot
  -XCF-1 Slower
  -XCF-3 Slower
  -Skywarden Slower
  -Added Antimatter Jewel (Basic material for XCom version of alien weapons)
  -Added Antimatter Bomb (Human version of the Alien high Explosive)
  -Added Elerium Grenade (Human version of the Alien Grenade, uses old alien grenade graphic)
  -Added Elerium Mine (Human version of the Alien Prox Mine)
  -Added Alloy Knife (Human version of the Alien Alloy Blade)
  -Added Plasma Sword (Human version of the Alien Plasma Sword)
  -Added human version of the small launcher and stun bombs that uses the old sprites
  -Removed Alien High Explosive, Alien Grenade, Alien Prox Grenade, Alloy Blade, and Plasma Sword from item equip list so they can be replaced with human versions.
  -Altered X5 Assault Rifle graphics again. Maybe one day I will be satisfied with it.
  -Fixed Skywarden Ramp Issue. Still working on the seeing through doors issue.
  -Fixed Alien Deployment entry for Large Scout having one too many item deployment lists
  -Fixed a bunch of little errors. Big thanks to Falko again for his awesome tools!
  -Tweaked white power armor sprite a bit for transparency issues.
  -Tweaked craft base sprites a bit.
  -Added Lite ruleset for those who like a quick paced game.
V0.80
  -Added Skywarden transport
  -Added Heavy Hypervelocity (Was the Cannon)
  -Altered Hypervelocity Cannon graphic
  -Altered X7 LMG graphics slightly
  -Removed height, width, and length settings from map deployments since this mod doesn't mess with them anyway. Except for Cydonia of course.
  -Replaced Psi Armor with the Nerve Suit. Has new graphics and pretty much the only fanservice this mod will ever have.
  -Personal armor is back! I simply felt it was too iconic to leave out. It even has the same stats.
V0.70
  -Changed the size of a bunch of clips and other items. Found it odd that a rifle clip takes up as much space in stores as a full rifle. This should free up some space and make keeping a large stockpile of ammo less punishing for the player.
  -Added Hypervelocity weapons as a sort of final tier of weapons for XCom. Currently do Laser Damage, but I would love to change it to something else down the road (waiting for custom damage types). I will add a craft weapon sometime in the future. Note: STR_HYPERVELOCITY_WEAPONS is a holdover from when these were suppose to be the next tier of ballistic weapons. Hence why most guns have HV in front of their internal name. This was later changed to just a better conventional gun. Basically all of the internal names are mixed up. I am hesitant to fix this as changing str names is messy and it is easy to miss them.
  -Finally added Overlord alien and a terrorist unit to go with them. The sprites may be tweaked in future versions, but it will have to do for now. Original sprites are hard.
  -Changed Cursor looks because I can!
  -Satellite Uplink Center upgraded to a 2x2 facility.
  -Added Hyperwave Uplink Center upgraded to a 2x2 facility.
  -Added Large Workshop.
  -Added Large Lab.
  -Added Large Psi Lab.
  -Added Corridor. This building is super useful for building new bases.
  -Added Security Checkpoint. This building breaks base defense.
  -Changed Alloy Ammo manufacturing unlock to also require the weapon tech as well to prevent ammo types from appearing for weapons the player hasn't unlocked yet. Ditto for laser weapon ammo types as well.
  -Big change. Reworked plasma conversion system. The new system allows a player to breakdown alien equipment into useful components for building plasma weapons. Alien Plasma weapons can no longer be converted over to human weapons on a 1 to 1 ratio. This adds some much needed scarcity as well as versatility to the mod, but I worry it may end up being too tedious. If it works then I may expand the system.
  -Change Alloy Ammo research requirements to appear earlier in the tech tree. I am considering adding alloy ammo for the starting weapons as well. Not sure yet.
  -Ditto for Laser Charged Cells and Elerium Batteries.
  -Changed height of terror mission deployment to account for terrain pack compatibility.
  -Tweaked Xeno Operations Armor weight reducing it by 1
  -Changed Hybrid file names in rules to match file name in order to be case sensitive.
  -Updated UFOpedia art for Alien Origins, the Martian Solution, and Cydonia or Bust. Has some nice symbolism in it.
  -Fixed melee weapons crashing in Alien hands by making clipsize: -1
  -Adjusted melee animations since the frames have been reduced by a recent nightly.
  -Added melee animation for stun rod.
  -Finally added a nice little melee animation for the Reaper.
  -Stole Dioxine's large living quarters and general stores as it compliments the other facilities well.
V0.62
  -Fixed armors not showing up on the manufacturing list. Curse you search and replace! I will get you back somehow...
  -Fixed a couple of graphic glitches in the inventory screen and ufopedia pages. I think one of the nightlies messed with the colors.
V0.61
  -Removed live sectoids accidentally left in for tech tree testing.
V0.6
  -Fixed black beret armor corpse turning green. Also fixed inventory screens.
  -Added Hybrid Terrorist Unit for Armored Sectoids.
  -Fixed Ubase being too well lit in final Cydonia Challenge. Was a hold over from when I was testing.
  -Added Charged Cell ammo for all laser weapons.
  -Added Elerium batteries for all laser weapons.
  -Moved string ID of interceptor so I don't have to alter starting gear on the craft rules. It was conflicting with the starting gear for the weapons rules.
  -Removed all vanilla firearms from starting gear. Classic Pistol and Rifle still available in store.
  -Fix STR issue with global radar facility not showing up in the build menu.
  -Altered Anti Material Rifle Clip weight down to four.
  -Added a small pistol, Magnum, Machine Pistol, and PDW for both starting gear and researchable XCom variants.
  -Lowered weight of Kevlar vests by 2. Might as well as the things are practically worthless as is. Shouldn't punish the player for wanting to look cool.
  -Fixed Sectoid research randomly popping up. Some junk code was causing it.
  -Changed all MAN_# to XOPS_# as well as XCOM_# to XOPS_XCOM_# to increase compatibility with other armor mods.
  -Added attack animation for the Zombie. Same as the Crysalid's though. Need to add a nice bite animation for the Reaper too.
  -Changed cyberdisc weapon bullet sprite to orange.
  -Updated UFOpedia art for both the Martian Solution and Cydonia or Bust.