Railgun mod 1.0 by robin
(for OpenXCOM)

Adds a new weapon to the game: a railgun that shoots AP slugs.
Researchable from the beginning,in this order:
Electromagnetic Weapon > Railgun > Railgun Slugs

Installation:
- open the "OpenXcom\data" folder;
- place the "Railgun" folder in the "Resources" folder;
- move "Railgun.rul" to the "Ruleset" folder;
- open "options.cfg" (found in "Documents\OpenXcom" folder in your Windows profile folder)
- Under "Rulesets:" somewhere in the file (usually top or bottom), add the following:
  - Railgun
- Save the changes to the .cfg and run the game.

Feel free to use or alter this mod in any way you wish.

Credits:
Ruleset: Solarius Scorch
Ammo Sprites: Dioxine
Special thanks: Ryskeliini fro the help