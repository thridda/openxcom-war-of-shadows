Raven Inteceptor (XCOM: Enemy Unknown) for UFO:EU v 1.0 (OpenXcom addon)


This addon includes the Raven interceptor from XCOM 2012.
Raven can be manufactured after "Improved Interceptor" has been researched.

Installation:
Extract to your data folder (..\OpenXcom\Data\) and enable the mod in game in the Options menu under Mods



Changelog:

1.0
- another minor tweak of hangar sprite
- added Slovak translation

0.2b
- you can no longer manufacture Raven without researching Improved Interceptor first

0.2
- new hangar sprite
- new ufopedia picture

0.1
- initial release

Credits:
Firaxis (Ufopedia image)
MickTheMage (all the crappy sprites and ruleset :))


